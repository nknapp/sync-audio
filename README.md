# Demo for multi-room audio using HTML5.

**Don't look at the code, it is awful, there are no tests. Everything is hacked down in one day to see if it 
  works at all**

This was just a test for figure out wether it actually works. And it did for me, pretty well.

## Concept

* Multiple players connect to one controller via WebRTC data connections (using [peerjs](https://peerjs.com/))
* Players synchronize time via WebRTC (which actually was not needed at all to get a decent result, and 
  the rest of the process does not use it.)
  * Player sends ping to the controller
  * Controller answers with current timestamp
  * Player computes time difference between own and controller-time taking into account half of the
    round-trip time as delay.
* Controller tells Players to download and prepare file (using fetch and the [Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API))
* Controller tells Players to play the file now

I thought it would be necessary to schedule "start" to a given timestamp, taking into account the 
different clocks of the client, but it worked fine like this.

I left the code in the repo anyway. Maybe I will clean this up some time.

