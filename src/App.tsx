import {useStore} from "@nanostores/solid";
import "./App.css";
import {Match, Switch, lazy} from "solid-js";
import {router} from "./store/router";
import {Messages} from "./components/Messages.tsx";

const Controller = lazy(() => import("./pages/Controller.tsx"));
const Player = lazy(() => import("./pages/Player.tsx"));

function App() {
    const page = useStore(router);

    // @ts-ignore
    return (
        <>
            <main>
                <Switch fallback={<div>Not found</div>}>
                    <Match when={page()!.route === "home"}>
                        <Controller/>
                    </Match>
                    <Match when={page()!.route === "player"}>
                        <Player controllerId={(page()!.params as any).controllerId}/>
                    </Match>
                </Switch>
                <Messages/>
            </main>
        </>
    );
}

export default App;
