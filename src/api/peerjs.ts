import {DataConnection, Peer} from "peerjs";
import {addMessage} from "../store/messages";
import {peerId} from "../store/peer-id";
import {nanoid} from "nanoid";
import {timeDifference} from "../store/timeDifference.ts";


export interface Controller {
    sendToAll(message: any): void;
    commands: Commands
    close(): void;
}

export function createController(): Controller {
    const connections: Set<DataConnection> = new Set();
    const peer = new Peer();
    peer.on("error", (error) => console.error(error));
    peer.on("open", (id) => {
        peerId.set(id)
    });
    peer.on("connection", (connection: DataConnection) => {
        connections.add(connection);
        connection.on("open", () => {
            connection.send("hello");
        });
        connection.on("error", (error) => console.error(error));
        connection.on("data", (data) => {
            const message = data as { name: "time", id: string}
            if (message.name === "time") {
                connection.send({
                    id: message.id,
                    result: Date.now()
                })
            }
        })

        connection.on("close", () => {
            connections.delete(connection);
        });
    });

    return {
        sendToAll(message: string) {
            console.log("Connections:", connections.size);
            for (const c of connections) {
                console.log("send to", c.label);
                c.send(message);
            }
        },
        commands: new Proxy({} as Commands, {
            get(_target, prop, _receiver) {
                return async (...args: any[]): Promise<void> => {
                    await Promise.all([...connections].map(c => {
                        return sendAndAwait(c, prop as string, [args])
                    }))
                }
            }
        }),
        close() {
            peer.disconnect();
        },
    };
}

interface Commands {
    load(url: string): Promise<void>
    start(time: number): Promise<void>
    stop(time: number): Promise<void>
    testMessage(message: string): Promise<void>
}

interface PlayerOptions extends Commands {
    getTime: () => number
    controllerId: string

}

type Message<T extends keyof Commands> = {
    name: T,
    id: string,
    args: Parameters<Commands[T]>
}

type AnyMessage = Message<keyof Commands>


export function createPlayer({getTime, controllerId, ...commands}: PlayerOptions) {
    const peer = new Peer();
    peer.on("error", (error) => {
        addMessage(error.toString());
    });

    peer.on("open", (id) => {
        peerId.set(id);
        addMessage("connect to " + controllerId);
        const connection = peer.connect(controllerId);
        connection.on("error", (error) => console.error(error));
        connection.on("open", () => {
            connection.send("hello boss");
            syncTime(connection, getTime)
        });
        connection.on("data", async (data) => {
            const message = data as AnyMessage
            console.log("player received ", data);
            await dispatchCommand(commands, message, connection)
        });
    })
}

async function dispatchCommand<T extends keyof Commands>(commands: Commands, message: Message<T>, connection: DataConnection) {
    if (!message.name || !commands[message.name]) {
        return
    }
    // @ts-ignore
    await commands[message.name](...message.args)
    connection.send({id: message.id})

}

async function syncTime(connection: DataConnection, getTime: () => number) {
    const now = getTime()
    const serverTime = await sendAndAwait(connection, "time")
    const duration = getTime() - now
    const halfPastDuration = now + duration / 2
    console.log(now, serverTime, duration)
    const difference = serverTime - halfPastDuration;
    timeDifference.set(difference)
    return difference
}

function sendAndAwait(connection: DataConnection, name: string, args?: any): Promise<any> {
    const id = nanoid()
    const result = new Deferred()
    console.log("send", Date.now(), args)
    const handler = (data: any) => {
        if (data.id === id) {
            connection.off("data", handler)
            console.log("done", Date.now())
            result.resolve(data.result)
        }
    }
    connection.on("data", handler)
    connection.send({id, name, args})
    return result.promise
}


class Deferred<T> {
    resolve!: (value: T) => void
    promise: Promise<T>

    constructor() {
        this.promise = new Promise<T>(resolve => {
            this.resolve = resolve
        })
    }
}