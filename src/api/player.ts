

export class AudioPlayer {
    context = new AudioContext()
    source: AudioBufferSourceNode = this.context.createBufferSource()

    async load(url: string) {
        console.log("load",url)
        const response = await fetch(url)
        this.source.buffer = await this.context.decodeAudioData(await response.arrayBuffer())
        this.source.connect(this.context.destination)
    }

    scheduleStart(time: number) {
        console.log("start",time)
        this.source.start(time)
    }

    stop(time: number) {
        this.source.stop(time)
    }
}
