import { createRouter } from "@nanostores/router";


const __BASE_PATH__ = "/sync-audio"
export const router = createRouter({
    home: `${__BASE_PATH__}/`,
    controller: `${__BASE_PATH__}/controller`,
    player: `${__BASE_PATH__}/player/:controllerId`
})
