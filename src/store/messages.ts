

import { atom } from 'nanostores';

export const messages = atom<string[]>([]);

export function addMessage(message: string): void {
    messages.set([...messages.get(), message].slice(-10))
}