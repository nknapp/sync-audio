import {createPlayer} from "../api/peerjs"
import {useStore} from "@nanostores/solid";
import {timeDifference} from "../store/timeDifference.ts";
import {AudioPlayer} from "../api/player.ts";
import {addMessage} from "../store/messages.ts";

export interface PlayerProps {
    controllerId: string
}

export default (props: PlayerProps) => {


    function connect() {
        const audioPlayer = new AudioPlayer()
        createPlayer({
                controllerId: props.controllerId,
                getTime: () => audioPlayer.context.currentTime,
                async load(url: string): Promise<void> {
                    addMessage("loading " + url)
                    return audioPlayer.load(url)
                },
                async start() {
                    addMessage("starting")
                    audioPlayer.scheduleStart(0)
                },
                async stop() {
                    addMessage("stopping")
                    audioPlayer.stop(0)
                },
                async testMessage(message: string) {
                    addMessage(message)
                }
            }
        )
    }

    const timeDifferenceStore = useStore(timeDifference)

    return <div>
        <button onClick={connect}>Connect</button>
        Connected {timeDifferenceStore()}
    </div>
}