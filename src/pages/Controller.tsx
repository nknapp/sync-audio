import {nanoid} from "nanoid";
import {Controller, createController} from "../api/peerjs";
import {peerId} from "../store/peer-id";
import {useStore} from "@nanostores/solid";
import {createEffect, createMemo, createSignal, Show} from "solid-js";
import QRCode from "qrcode"
import {addMessage} from "../store/messages.ts";
import radioIcon from "../assets/radio-fm-icon.svg"
import {getPagePath} from "@nanostores/router";
import {router} from "../store/router.ts";


export default () => {
    const id = useStore(peerId)
    let controller: Controller | null = null
    const [controllerStarted, setControllerStarted] = createSignal(false)

    const playerConnectUrl = createMemo(() => {
        let pagePath = getPagePath(router, "player", {controllerId: id()});
        return document.location.origin + pagePath;
    })
    const [canvas, setCanvas] = createSignal<HTMLCanvasElement>()
    createEffect(() => {
        const c = canvas()
        if (c == null) return
        QRCode.toCanvas(c, playerConnectUrl())
    })

    async function startAll() {
        let url = document.location.origin + "/sync-audio/music.mp3";
        addMessage("loading " + url)
        await controller!.commands.load(url)
        addMessage("starting")
        await controller!.commands.start(0)
    }

    async function testMessage() {
        const message = nanoid(4)
        addMessage("Sending " + message)
        await controller!.commands.testMessage(message)
    }

    function startController() {
        controller = createController();
        setControllerStarted(true)
    }

    return (
        <div>
            <img style={{float: "right", margin: "1rem"}} alt="Radio icon" src={radioIcon} width={100} height={100}/>
            <h1>Audio-Sync Demo</h1>
            <p>I wanted to see if we can use the Web-Audio-API and WebRTC to perfectly synchronize music replay,
                like in a multi-room setup.
            </p>
            <p>I was surprised to see that an exact clock-synchronization wasn't necessary to achieve a good
                result. See <a href={"https://gitlab.com/nknapp/sync-audio"}>the repository at Gitlab for details.</a>
            </p>
            <Show when={!controllerStarted()}>
                <h2>Step 0: Connect to peer.js cloud</h2>
                <p><strong>Privacy notice:</strong> This button creates a connection to <a
                    href={"https://peerjs.com/peerserver"}
                >the peer.js cloud server</a> in order to broker WebRTC between your devices. I don't think it is
                    problematic,
                    but you may think differently.</p>
                <button onClick={startController}>Start controller</button>
            </Show>
            <Show when={controllerStarted()}>
                <h2>Step 1: Connect multiple players</h2>
                <section>
                    <aside>
                        <h3>This computer</h3>
                        <p>Open this link in a new <strong>incognito</strong> window: <a href={playerConnectUrl()}>Player
                            (Worker)</a></p>
                    </aside>
                    <aside>
                        <h3>Mobile devices</h3>
                        <p>scan this QR code with multiple mobile devices</p>
                        <canvas ref={setCanvas}/>
                    </aside>
                </section>
                <h2>Step 2: Send a test-message</h2>
                <button onClick={testMessage}>Send Test Message</button>
                <p>This message show appear on all clients</p>
                <h2>Step 3: Start playing</h2>
                <button onClick={startAll}>Play</button>
                <h2>Step 4: Stop playing</h2>
                <p>If you don't like my music...</p>
                <button onClick={() => controller!.commands.stop(0)}>Stop</button>
                <h2>Step 5: Reload the page</h2>
                <p>This is a dirty PoC, so you can only use it once.</p>
            </Show>
        </div>
    );
};
