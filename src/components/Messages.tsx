import { useStore } from "@nanostores/solid";
import { messages } from "../store/messages";
import { For } from "solid-js";

export const Messages = () => {
  const messageStore = useStore(messages);

  return (
    <ul>
      <For each={messageStore()}>
        {(message) => {
          return <li>{message}</li>;
        }}
      </For>
    </ul>
  );
};
